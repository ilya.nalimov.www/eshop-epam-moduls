using System;
using System.IO;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace eshop_module_8
{
    public static class OrderItemsReserver
    {
        [FunctionName("OrderItemsReserver")]
        public static void Run([ServiceBusTrigger("qu-orders", Connection = "eshopQueueConnectionString")] string myQueueItem, Int32 deliveryCount,
            [Blob("blobeshopcontainer/{rand-guid}", Connection = "StorageConnectionString")] TextWriter outputContainer, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}:{deliveryCount}");
            throw new Exception("check retry"); // to check retry policy
            outputContainer.Write(myQueueItem);
           
        }
    }
}
