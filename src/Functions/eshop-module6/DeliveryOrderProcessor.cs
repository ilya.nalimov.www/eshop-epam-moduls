using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Web.Http;

namespace eshop_module6
{
    public static class DeliveryOrderProcessor
    {
        public class Order
        {
            public string Address { get; set; }
            public string BuyerId { get; set; }
            public decimal Total { get; set; }
            public int OrderId { get; set; }

            public Item[] Items { get; set; }
        }
        public class Item
        {
            public int CatalogItemId { get; set; }
            public int Quantity { get; set; }
        }
        [FunctionName("DeliveryOrderProcessor")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] Order req,
            [CosmosDB(
        databaseName: "Eshop",
        collectionName: "Orders",
        ConnectionStringSetting = "bagron_DOCUMENTDB")]IAsyncCollector<dynamic> documentsOut,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string orderJsonString = JsonConvert.SerializeObject(req);
            try
            {
                // Add a JSON document to the output container.
                await documentsOut.AddAsync(new
                {
                    orderId = req.OrderId,
                    order = orderJsonString
                });
            }catch(Exception ex)
            {
                return new ExceptionResult(ex, true);
            }
            

            string responseMessage = $"Hello,Items.Count: {req.Items.Length}. This HTTP triggered function executed successfully.";

            return new OkObjectResult(responseMessage);
        }
    }
}
