﻿using Ardalis.GuardClauses;
using Microsoft.Azure.ServiceBus;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Entities.BasketAggregate;
using Microsoft.eShopWeb.ApplicationCore.Entities.OrderAggregate;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Specifications;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class OrderService : IOrderService
    {
        private readonly IAsyncRepository<Order> _orderRepository;
        private readonly IUriComposer _uriComposer;
        private readonly HttpClient _httpClient;
        private readonly IAsyncRepository<Basket> _basketRepository;
        private readonly IAsyncRepository<CatalogItem> _itemRepository;
        //private static readonly HttpClient client = new HttpClient();
        const string connQuString = "Endpoint=sb://queue-module7.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=nm0Mi9QdhnuDon0mC1dTrBzGpiDjzTscsOPTMo2lKj0=";
        const string quName = "module8queue";
        private static  IQueueClient quclient;

        public OrderService(IAsyncRepository<Basket> basketRepository,
            IAsyncRepository<CatalogItem> itemRepository,
            IAsyncRepository<Order> orderRepository,
            IUriComposer uriComposer,
            HttpClient httpClient)
        {
            _orderRepository = orderRepository;
            _uriComposer = uriComposer;
            _httpClient = httpClient;
            _basketRepository = basketRepository;
            _itemRepository = itemRepository;
       
        }
        public class MyOrder
        {
            public string BuyerId { get; set; }
            public string Address { get; set; }
            public decimal Total { get; set; }
            public BasketItem[] Items { get; set; }
            public int OrderId { get; internal set; }
        }

        public async Task CreateOrderAsync(int basketId, Address shippingAddress)
        {
            var basketSpec = new BasketWithItemsSpecification(basketId);
            var basket = await _basketRepository.FirstOrDefaultAsync(basketSpec);

            Guard.Against.NullBasket(basketId, basket);
            Guard.Against.EmptyBasketOnCheckout(basket.Items);

            var catalogItemsSpecification = new CatalogItemsSpecification(basket.Items.Select(item => item.CatalogItemId).ToArray());
            var catalogItems = await _itemRepository.ListAsync(catalogItemsSpecification);

            var items = basket.Items.Select(basketItem =>
            {
                var catalogItem = catalogItems.First(c => c.Id == basketItem.CatalogItemId);
                var itemOrdered = new CatalogItemOrdered(catalogItem.Id, catalogItem.Name, _uriComposer.ComposePicUri(catalogItem.PictureUri));
                var orderItem = new OrderItem(itemOrdered, basketItem.UnitPrice, basketItem.Quantity);
                return orderItem;
            }).ToList();

            var order = new Order(basket.BuyerId, shippingAddress, items);

            await _orderRepository.AddAsync(order);
            var orderToBlob = new MyOrder();
            orderToBlob.BuyerId = basket.BuyerId;
            orderToBlob.OrderId = order.Id;
            orderToBlob.Address = order.ShipToAddress.GetFullAddress();
            orderToBlob.Total = order.Total();
            orderToBlob.Items = basket.Items.ToArray();
            var myContent = JsonConvert.SerializeObject(orderToBlob);           
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);

            var mess = new Message(buffer);
            const string connQuString = "Endpoint=sb://qu-final-eshop-task.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=fvg7+74dUdZ1g/agCeZ2y90T876fy1M5lAd5ElUujr0=";
            const string quName = "qu-orders";

            quclient = new QueueClient(connQuString, quName);
            await quclient.SendAsync(mess);
            await quclient.CloseAsync();

            var byteContent = new ByteArrayContent(buffer);
            
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");          

            var response = await _httpClient.PostAsync("https://eshopfinaltaskapp.azurewebsites.net/api/DeliveryOrderProcessor?", byteContent);

            var responseString = await response.Content.ReadAsStringAsync();
        }
    }
}
